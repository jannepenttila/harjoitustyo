/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko.pkg8;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author jannepenttila
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Button textSender;
    @FXML
    private TextField textField;
    @FXML
    private TextField liveSender;
    @FXML
    private Label label2;
    @FXML
    private TextField saveTextField;
    @FXML
    private Label label3;
    @FXML
    private Button loadTextButton;
    @FXML
    private Button SaveTextButton;
    
    public String text2File;
    
    public String fileName;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("Hello Wordl!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void sendText(ActionEvent event) {
        label2.setText(textField.getText());
    }


    @FXML
    private void liveSend(KeyEvent event) {
        label.setText(liveSender.getText());
    }

    @FXML
    private String loadAndInfo(ActionEvent event) {
        label3.setText("Anna seuraavaksi nimi tiedostolle johon haluat tallentaa.");
        text2File = saveTextField.getText();
        saveTextField.clear();
        return text2File;    
    }
    
    public void saver() throws IOException{
        fileName = saveTextField.getText();
        File file = new File(fileName);
        FileWriter writer;
        try {
            writer = new FileWriter(file, true);
            PrintWriter printer = new PrintWriter(writer);
            printer.append(text2File);
            printer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        label3.setText("Tallennettu tiedostoon " + fileName);
    
    
    }
    
    

    @FXML
    private void saveText2File(ActionEvent event) throws IOException{
        saver();
        saveTextField.clear();

        
        
    }


    
}
