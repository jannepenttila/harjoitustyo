/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

/**
 *
 * @author jannepenttila
 */
public class smartpost {
    
    private final String code;
    private final String city;
    private final String address;
    private final String availability;
    private final String postoffice;
    private final String lat;
    private final String lng;
    
    
    // Constructor for smartpost object. 
    public smartpost(String a, String b, String c, String d, String e, String f, String g) {
         code = a;
         city = b;
         address = c;
         availability = d;
         postoffice = e;
         lat = f;
         lng = g;
    }

    // Getters for all the attributes
    public String getCode() {
        return code;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostoffice() {
        return postoffice;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
    
    // This is used to get city and address as a name for object.
    @Override
    public String toString(){
        
        return city+", "+address; 
    }
    
    
    
}
