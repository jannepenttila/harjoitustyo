/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import java.util.ArrayList;

/**
 *
 * @author jannepenttila
 */
public class storage {
    
    /**
     *
     */
    // Static ArrayList which holds all the created packages. 
    public static ArrayList<postpackage> packages = new ArrayList<>();

    public storage() {
    }
    // Method to add objects. 
    public void addObject(postpackage n){
    
        packages.add(n);
        
    }
    
    // Getter for packages.
    public ArrayList<postpackage> getPackages(){
        
        return packages;
    }
    
    
}
