/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import java.util.ArrayList;

/**
 *
 * @author jannepenttila
 */
public class postpackage {
    
    protected float volume;
    protected float weight;
    protected String name;
    protected boolean fragility;
    protected float fromLat;
    protected float fromLng;
    protected float toLat;
    protected float toLng;
    protected int speed;
    protected int maxDist;
    protected int maxWeight;
    protected int maxVolume;
    protected int minWeight;
    protected int minVolume;
    protected int chooseClass;
    // This stores to path of the package. 
    ArrayList<Float> path = new ArrayList<>();
    
    // Constructor for postpackage. 
    public postpackage(float a, float b, String c, boolean d, float e, float f, float g, float h) {
        volume = a;
        weight = b; 
        name = c;
        fragility = d;
        
        // Path coordinates added to path ArrayList
        // This is the form which javascritp uses when drawing the path on map. 
        path.add(e);
        path.add(f);
        path.add(g);
        path.add(h);

    }
    
    // Getters for all the attributes. 
    public float getVolume() {
        return volume;
    }

    public float getWeight() {
        return weight;
    }

    public ArrayList<Float> getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public boolean getFragility() {
        return fragility;
    }

    public int getSpeed() {
        return speed;
    }

    public int getMaxDist() {
        return maxDist;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public int getMinWeight() {
        return minWeight;
    }
    
    public int getChooseClass() {
        
        return chooseClass;
    }

    public int getMinVolume() {
        return minVolume;
    }

    public int getMaxVolume() {
        return maxVolume;
    }
    // This is used to show name of the object
    @Override
    public String toString() {
        
        return name;
    
    }
     
}
