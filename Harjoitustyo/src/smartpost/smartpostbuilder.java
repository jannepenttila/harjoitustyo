/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jannepenttila
 */
public class smartpostbuilder {
    
    private Document sPinfo;
    public static HashMap<String, smartpost> smartPostData;
    private String x;
    

    // Constructor.
    // Several sources were used to makes this work:
    // https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
    // https://www.tutorialspoint.com/java_xml/java_dom_parse_document.htm
    // Course material: Erno Vanhalas lecture videos
    public smartpostbuilder () throws ParserConfigurationException, SAXException{
        
        
        try {
            // Xml file opened.
            File smartPostInfo = new File("fi_apt.xml");
            
            // According the lecture videos, specifics on builders and factories belong to a different course. 
            // So... no comments.
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            // dBuilders method "parse" used to parse xml file.
     
            sPinfo = dBuilder.parse(smartPostInfo);
            
            // Normaliza parsed data
            sPinfo.getDocumentElement().normalize();
            // New hasmap created to store all the information
            smartPostData = new HashMap();
            // Parser function called
            parser();
            
        } catch (IOException ex) {
            Logger.getLogger(smartpostbuilder.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
    private void parser (){
        
        int id;
        String id_string;
        
        // getElementsByTagName used to get all the information under "place".
        // In XML -file all the information for one smartpost is stored under identifier "place". 
        // So, one node in nodes is all the information of one smartpost. 
        NodeList nodes = sPinfo.getElementsByTagName("place");
        
        // For loop from i to number of nodes (number of smarposts!)
        for(int i = 0; i < nodes.getLength(); i ++){
            
            // node is item i in nodes (item i in nodes is all the info of one smartpost) 
            Node node = nodes.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element e = (Element) node;
               
                id = i;
                
                id_string = Integer.toString(id);
                // New smartpost contructed. getElementsByTagName -method used to bring right value from node.
                smartpost sp = new smartpost(
                        e.getElementsByTagName("code").item(0).getTextContent(),
                        e.getElementsByTagName("city").item(0).getTextContent(),
                        e.getElementsByTagName("address").item(0).getTextContent(),
                        e.getElementsByTagName("availability").item(0).getTextContent(),
                        e.getElementsByTagName("postoffice").item(0).getTextContent(),
                        e.getElementsByTagName("lat").item(0).getTextContent(),
                        e.getElementsByTagName("lng").item(0).getTextContent());
                // Created smartpost added to HashMap smartPostData, id_string used as identifier.
                smartPostData.put(id_string, sp);
                  
            }    
        }
    
    } 
}
