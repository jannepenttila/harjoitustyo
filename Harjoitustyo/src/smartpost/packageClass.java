/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartpost;

/**
 *
 * @author jannepenttila
 */
public class packageClass extends postpackage {
    
   
    // This gives features of chosen class to postpackage. When contructing new postpackage, int i tells which class to use. 
    public packageClass(float a, float b, String c, boolean d, float e, float f, float g, float h, int i) {
        super(a, b, c, d, e, f, g, h);
        
        chooseClass = i;
        
        
        // Switch - case -statemant used to determinte which attributes package gets. 
        switch (chooseClass) {
            case 1:
                speed = 1;
                maxDist = 150;
                maxWeight = 3000;
                maxVolume = 3000;
                minWeight = 0;
                minVolume = 0;
                break;
            case 2:
                speed = 2;
                maxWeight = 30;
                maxDist = 2000;
                maxVolume = 40;
                minWeight = 0;
                minVolume = 0;
                break;
            case 3:
                speed = 3;
                minWeight = 30;
                maxDist = 2000;
                maxWeight = 3000;
                maxVolume = 3000; 
                minVolume = 30;
                
                break;
            default:
                break;
        }
    }

        
        
      
 
    
}
