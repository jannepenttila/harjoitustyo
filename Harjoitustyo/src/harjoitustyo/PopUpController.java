/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import static harjoitustyo.FXMLDocumentController.usageLog;
import static harjoitustyo.FXMLDocumentController.errorText;

/**
 * FXML Controller class
 *
 * @author jannepenttila
 */
public class PopUpController implements Initializable {

    @FXML
    private Label infoTextLabel;
    @FXML
    private Button okButton;
   

    /**
     * Initializes the controller class.
     * @param url
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // This pop up shows string errorText, which is static and can be changed from other classes. 
        infoTextLabel.setText(errorText);

    }

    @FXML
    private void killWindowAction(ActionEvent event) {
        
        // Window closes when this action is used. 
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();   
    }
    
    
    
    
}
