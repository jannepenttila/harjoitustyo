/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;
import static harjoitustyo.FXMLDocumentController.usageLog;
import static java.lang.Math.log;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jannepenttila
 */
public class LogController implements Initializable {

    @FXML
    private Button closeButton;
    @FXML
    private TextArea logDisplay;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // logDisplay not editable
        logDisplay.setEditable(false);
 
        // Content of the usageLog is added to logDisplay text area.
        for(int i=0; i< usageLog.size() ;i++){
                
                logDisplay.setText(logDisplay.getText()+usageLog.get(i));
            }
    }    

    @FXML
    private void closeAction(ActionEvent event) {
        
        // Window closes when closeAction used. 
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close(); 
    }
  
    
}
