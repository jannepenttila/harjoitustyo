/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import smartpost.object1;
import smartpost.postpackage;
import smartpost.smartpostbuilder;
import smartpost.smartpost;
import smartpost.storage;
import static smartpost.storage.packages;
import harjoitustyo.PopUpController;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import static smartpost.smartpostbuilder.smartPostData;

/**
 *
 * @author jannepenttila
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private WebView mapView;
    @FXML
    private ChoiceBox<smartpost> smartPostList;
    @FXML
    private ChoiceBox<postpackage> packageList;
    @FXML
    private Button addToMapAction;
    @FXML
    private Button addPackage;
    @FXML
    private Button emptyMapButton;
    @FXML
    private Button sendPackageButton;
    
    static String errorText;
    
    public static ArrayList<String> usageLog;
    
    
    @FXML
    private Button logButton;
    @FXML
    private Button exitButton;

    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // Here we construct smartpostbuiler, which reads xml -file and constructs all smarpost objects.
        // Smartpostbuilder contains static HashMap, all the smartpost objects will be added on that HashMap.
        
        try {
            smartpostbuilder sp = new smartpostbuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Declare usageLog. All the usage notes will be added on this ArrayList.
        
        usageLog = new ArrayList();
        
        // Set null values to these combo boxes to avoid errors. 
        packageList.setValue(null);
        smartPostList.setValue(null);
        
        // index.html loaded to mapView for usage.
        
        mapView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        // smartPostList is a combo box, here all the smartpost objects are added on it. 
        int id;
        String id_string;

        for(int i = 0; i < 500; i ++){

            id = i;

            id_string = Integer.toString(id);

            smartPostList.getItems().add(smartPostData.get(id_string));

        }
  
    }    


    @FXML
    private void addPackageAction(ActionEvent event) {
        
        // When addPackage button is pushed, new window opens (FXMLpackageAdder).
        
        try {
            Stage packageAdder = new Stage();
            
            Parent page = FXMLLoader.load(getClass().getResource("FXMLPackageAdder.fxml"));
            
            Scene scene = new Scene(page);
            
            packageAdder.setScene(scene);
            
            packageAdder.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    @FXML
    private void addPointAction(ActionEvent event) {
        
        // This action adds locations of smarpost objects to map.
 
       String injection;
       
       // If condition to avoid errors, if smartPostlist value is not null, javascript is executed.  
       // "injection" is used to help execution. 
       // All the parameters which addPoint -script needs,
       // are taken with several different methods from the object chosen in "smarPostList". 
       if (smartPostList.getValue() != null){
       
        injection =  "document.goToLocation('"
                +smartPostList.valueProperty().getValue().getAddress()+", "+
                smartPostList.valueProperty().getValue().getCity()+"', 'Auki: "
                +smartPostList.valueProperty().getValue().getAvailability()+"', 'blue')";

         mapView.getEngine().executeScript(injection);
        
        }
    }

    @FXML
    private void emptyMapAction(ActionEvent event) throws ParserConfigurationException, SAXException {
       
        // Map is cleared
        mapView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void sendPackageAction(ActionEvent event) {
        
       String injection;
       
       double distance;
       
       // Ok... then comes the REALLY messy part. 
       
        // Nothing happens if sendPackage -button is pressed when nothing is chosen on packageList.
        if(packageList.getValue() != null){
            
            // Older routes are cleared with this script. 
            mapView.getEngine().executeScript("document.deletePaths()");
            
            injection =  "document.createPath("
            +packageList.valueProperty().getValue().getPath()+", 'Red', "
            +packageList.valueProperty().getValue().getSpeed()+")";
            
            
            // This javascript returns the distance between two points, this distance is saved on variable "distance".
            // Main function of this is to draw the route on map. 
            distance = (double) mapView.getEngine().executeScript(injection);
           
            // Sended package is removed from storage
            packages.remove(packageList.getValue());
            
            // Following string is written/added on usageLog ArrayList. 
            usageLog.add("Sending package containing: " + packageList.valueProperty().getValue().getName()
                    +" Will travel distance: " + distance
                    +" Storage containing after sending: "+packages.size()+"\n");
            
            // Following conditions will check if the package is properly constructed.
            
            
            // If packed item is fragile and class 1 or 3 is used to send it, it will break.
            // A pop up window will inform user. 
            if(packageList.valueProperty().getValue().getFragility() == true
            && packageList.valueProperty().getValue().getChooseClass() == 1
            || packageList.valueProperty().getValue().getChooseClass() == 3
            &&packageList.valueProperty().getValue().getFragility() == true){
                
                // Note is added to usageLog.
                usageLog.add("Package broke.\n");
                
                
                // Popup window set up. 
                // errorText is a static string, it's value can be changed like below to display certain error message.
                // Similar logic is used on all popup windows on this program to show error messages. 
                try {
                    
                    errorText = "Your package was send, butt will be broken at the destination.\n"
                            +"Hint: don't send fragile items in classes 1 or 3. Use class 2."; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Your package broke.");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                } 
        
            }
            // If items weight exceeds the weight limit of the class used, package will not be sended. 
            if (packageList.getValue().getWeight() > packageList.getValue().getMaxWeight()){
                // Package is still removed from storage. 
                packages.remove(packageList.getValue());
                
                // Deletepath -script is used to illustrate the termination.
                mapView.getEngine().executeScript("document.deletePaths()");
                
                // Note is added to usageLog.
                usageLog.add("Problems with weight, cannot send.\n");
                
                // Popup window set up. 
                try {
                    
                    errorText = "You have chosen class: " + packageList.getValue().getChooseClass() +
                            ", weight limit for that class is " +  packageList.getValue().getMaxWeight()+".\n"
                            +"Your item weighs: " + packageList.getValue().getWeight()+".\n"
                            +"Package cannot be send and will be deleted from stroage." ; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Problem sending package");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }    
            
            // If items volume exceeds the volume limit of the class used, package will not be sended.    
            } else if(packageList.getValue().getVolume() > packageList.getValue().getMaxVolume()){
                // Package is still removed. 
                packages.remove(packageList.getValue());
                // Delete path to illustrate cancellation. 
                mapView.getEngine().executeScript("document.deletePaths()");
                
                // Add note to the usageLog. 
                usageLog.add("Problems with volume, cannot send.\n");
                
                // Pop up -window set up. 
                try {
                    
                    errorText = "You have chosen class: " + packageList.getValue().getChooseClass() +
                            ", volume limit for that class is " +  packageList.getValue().getMaxVolume()+".\n"
                            +"Your item's volume is: " + packageList.getValue().getVolume()+".\n"
                            +"Package cannot be send and will be deleted from stroage." ; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Problem sending package");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            // Same logic, but now the other way around. Some classes have minium volume and weight.     
            } else if(packageList.getValue().getVolume() < packageList.getValue().getMinVolume()){
                packages.remove(packageList.getValue());
                
                mapView.getEngine().executeScript("document.deletePaths()");
                
                usageLog.add("Problems with volume, cannot send.\n");
                
                try {
                    
                    errorText = "You have chosen class: " + packageList.getValue().getChooseClass() +
                            ", minimiun volume for that class is " +  packageList.getValue().getMaxVolume()+".\n"
                            +"Your item's volume is: " + packageList.getValue().getVolume()+".\n"
                            +"Package cannot be send and will be deleted from stroage." ; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Problem sending package");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            // Same logic as before, but now other way around.     
            } else if(packageList.getValue().getWeight() < packageList.getValue().getMinWeight()){
                
                packages.remove(packageList.getValue());
                
                mapView.getEngine().executeScript("document.deletePaths()");
                
                usageLog.add("Problems with weight, cannot send.\n");
                
                try {
                    
                    errorText = "You have chosen class: " + packageList.getValue().getChooseClass() +
                            ", minimiun weight for that class is " +  packageList.getValue().getMinWeight()+".\n"
                            +"Your item's weight is: " + packageList.getValue().getWeight()+".\n"
                            +"Package cannot be send and will be deleted from stroage." ; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Problem sending package");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            // Still same logic, if the distance between smartposts exceed the distance limit, cancellation happens.     
            } else if (distance > packageList.getValue().getMaxDist()){
                
                packages.remove(packageList.getValue());
                mapView.getEngine().executeScript("document.deletePaths()");
                
                usageLog.add("Problems with distance, cannot send.\n");
                
                try {
                    
                    errorText = "You have chosen class: " + packageList.getValue().getChooseClass() +
                            ", maximum distance for that class is " +  packageList.getValue().getMaxDist()+" km.\n"
                            +"Distance between smarpost's now is: " + distance + " km.\n"
                            +"Package cannot be send and will be deleted from stroage." ; 
                    Stage popUp = new Stage();
                    Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                    Scene scene = new Scene(page);
                    popUp.setScene(scene);
                    popUp.setTitle("Problem sending package");
                    popUp.show();

                } catch (IOException ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
           
        }
     
    }

    @FXML
    private void showStorageAction(MouseEvent event) {
        
        
        // When packageList is pressed, all the stored packages are added. 
        packageList.getItems().clear();
        packageList.getItems().addAll(packages);
        
    }

    @FXML
    private void openLogButton(ActionEvent event) {
        
        
        // This opens new window which shows the content of usageLog. 
        try {
      
            Stage log = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("log.fxml"));
            Scene scene = new Scene(page);
            log.setScene(scene);
            log.setTitle("Log window");
            
            log.show();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void exitAction(ActionEvent event) {
        
        // This if the "official way to exit program.
        
        // When exitButton is pressed, all the content in usageLog ArrayList is written to a file. 
        
        String fileName;
        
        fileName = "log_htyo.txt";
        
        // Number of stored items is added to usageLog. 
        
        // This tells how many packages was not send or otherwise terminated. 
        usageLog.add("Shutting down, storage containing: " + packages.size());
        // File opened for writing
        File file = new File(fileName);
        FileWriter writer;
        try {
            writer = new FileWriter(file, true);
            PrintWriter printer = new PrintWriter(writer);
            // For -loop is used to add all the content in usageLog. 
            for(int i=0; i< usageLog.size() ;i++){
                
                printer.append(usageLog.get(i));
            }
            
            // Writing closed.
            printer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // Main window is closed.
          
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }
    
}
