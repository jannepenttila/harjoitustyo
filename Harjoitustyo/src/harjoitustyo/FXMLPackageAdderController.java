/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import smartpost.object1;
import smartpost.object2;
import smartpost.object3;
import smartpost.object4;
import smartpost.object_n;
import smartpost.postpackage;
import smartpost.smartpostbuilder;
import smartpost.smartpost;
import static smartpost.smartpostbuilder.smartPostData;
import static smartpost.storage.packages;
import static harjoitustyo.FXMLDocumentController.usageLog;
import static harjoitustyo.FXMLDocumentController.errorText;
/**
 * FXML Controller class
 *
 * @author jannepenttila
 */
public class FXMLPackageAdderController implements Initializable {
    

    @FXML
    private ChoiceBox<String> objectList;
    @FXML
    private TextField nameField;
    @FXML
    private ChoiceBox<smartpost> fromList;
    @FXML
    private ChoiceBox<smartpost> toList;
    @FXML
    private ChoiceBox<Integer> classChoiceBox;
    @FXML
    private Label infoLabel;
    @FXML
    private TextField volumeField;
    @FXML
    private TextField weightField;
    @FXML
    private Button createPackageButton;
    @FXML
    private Button closeButton;
    @FXML
    private CheckBox fragilityChooser;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // Default values setted to avoid errors.
        objectList.setValue("empty");
        classChoiceBox.setValue(1);
        toList.setValue(null);
        fromList.setValue(null);
        
        
        // Info text setted on infoLabel.
        infoLabel.setText("Class info:\n\n1. class:\n- Max dist: 150km\n- Weight limit: none\n- Volume limit: none\n\n"
                + "2. class:\n- Distance not limited\n- Weight limit: 30\n- Volume limit: 40\n\n"
                + "3. luokka:\n- Distance not limited\n- Weight must exceed: 30\n- Volume must exceed: 30");
        
        // Class choices added classChoiceBox.
        classChoiceBox.getItems().add(1);
        classChoiceBox.getItems().add(2);
        classChoiceBox.getItems().add(3);
        
        // Ready made items added to objectList.
        objectList.getItems().add("empty");
        objectList.getItems().add("Eikin vilmit");
        objectList.getItems().add("Kimmon denssit");
        objectList.getItems().add("Kohosen karjala 100-pack");
        objectList.getItems().add("Tolosen samurai vilmit");
     
        // All the smartpost objects are added to "toList" and "fromList"
        // smartPostDate is static list, new contruction is not needed.
        int id;
        String id_string;

        for(int i = 0; i < 500; i ++){

            id = i;
            
            id_string = Integer.toString(id);

            toList.getItems().add(smartPostData.get(id_string));
            fromList.getItems().add(smartPostData.get(id_string));

        }
     
    }    


    @FXML
    private void createPackageAction(ActionEvent event) {
        
        String object;
        
        boolean checker = true;
        
        // If user creates his/hers own item, volume and weight field must contain only floats or ints.
        // These two if conditions checks if those two fields are correctly filled. 
        // If not correctly filled, checker will be false and error is shown to user. 
        
        if(volumeField.getText().length() > 0){
        
            try{

                Float.parseFloat(volumeField.getText());

            } catch(NumberFormatException e) {

                checker = false;

            }
        
        }
        
        if(weightField.getText().length() > 0){
        
            try{

                Float.parseFloat(weightField.getText());

            } catch(NumberFormatException e) {

                checker = false;

            }
        
        }
   
       
        // This if - else statements check if user is following rules when adding packages. 
        
        // Rules are: 
        //- If user chooses ready made item, to and from must be filled. 
        //- When using items from objectList, name, weight and volume -fields must be empty.
        //- When adding own package, objectList must be empty. to and from must be filled. 
        //- If not, error message is shown. 
        if(objectList.getValue() != "empty" && nameField.getText().length() == 0
                && weightField.getText().length() == 0 && volumeField.getText().length() == 0
                && toList.getValue() != null && fromList.getValue() != null
                && toList.getValue() != fromList.getValue()) 
            
            // Switch - case statement is used to choose which object to create.
            // Several methods are used to get values from the chosen option
            switch (objectList.getValue()) {
            case "Eikin vilmit":
                //New object is constructed.
                object1 o1 = new object1(
                Float.parseFloat(fromList.valueProperty().getValue().getLat()),
                Float.parseFloat(fromList.valueProperty().getValue().getLng()),
                Float.parseFloat(toList.valueProperty().getValue().getLat()),
                Float.parseFloat(toList.valueProperty().getValue().getLng()),
                classChoiceBox.getValue()
                );
                // Package added to storage.
                packages.add(o1);
                // Information below is added to usageLog.
                usageLog.add("Added package containing: " + objectList.getValue()
                    +" Going from: "+fromList.getValue() +" to: " + toList.getValue()
                    +" Storage containing: "+packages.size()+"\n");
 
                break;
                // Similar logic here.
            case "Kimmon denssit":
                object2 o2 = new object2(
                Float.parseFloat(fromList.valueProperty().getValue().getLat()),
                Float.parseFloat(fromList.valueProperty().getValue().getLng()),
                Float.parseFloat(toList.valueProperty().getValue().getLat()),
                Float.parseFloat(toList.valueProperty().getValue().getLng()),
                classChoiceBox.getValue()
                );
                packages.add(o2);
                
                usageLog.add("Added package containing: " + objectList.getValue()
                    +" Going from: "+fromList.getValue() +" to: " + toList.getValue()
                    +" Storage containing: "+packages.size()+"\n");

              
                break;
                // Similar logic here.
            case "Kohosen karjala 100-pack":
                object3 o3 = new object3(
                Float.parseFloat(fromList.valueProperty().getValue().getLat()),
                Float.parseFloat(fromList.valueProperty().getValue().getLng()),
                Float.parseFloat(toList.valueProperty().getValue().getLat()),
                Float.parseFloat(toList.valueProperty().getValue().getLng()),
                classChoiceBox.getValue()
                );
                
                packages.add(o3);
                
                usageLog.add("Added package containing: " + objectList.getValue()
                    +" Going from: "+fromList.getValue() +" to: " + toList.getValue()
                    +" Storage containing: "+packages.size()+"\n");
                              
                break;
                // Similar logic here.
            case "Tolosen samurai vilmit":
                object4 o4 = new object4(
                Float.parseFloat(fromList.valueProperty().getValue().getLat()),
                Float.parseFloat(fromList.valueProperty().getValue().getLng()),
                Float.parseFloat(toList.valueProperty().getValue().getLat()),
                Float.parseFloat(toList.valueProperty().getValue().getLng()),
                classChoiceBox.getValue()
                );
                
                packages.add(o4);
                
                usageLog.add("Added package containing: " + objectList.getValue()
                    +" Going from: "+fromList.getValue() +" to: " + toList.getValue()
                    +" Storage containing: "+packages.size()+"\n");
                
                break;
   
            default:
                
                break;
        // Next section is used when user wants to make his/hers own item.         
        } else if (objectList.getValue() == "empty" && nameField.getText().length() != 0
                && weightField.getText().length() != 0 && volumeField.getText().length() != 0
                && toList.getValue() != null && fromList.getValue() != null && checker == true
                && toList.getValue() != fromList.getValue() ) {
            
            // New object is created. We now use object_n, because user wants to create his/hers own item. 
            object_n p1 = new object_n(
                    
            // Several methods are used to take information out of the chosen options. Methods return string, which must be casted to float.
            
            Float.parseFloat(volumeField.getText()),
            Float.parseFloat(weightField.getText()),
            nameField.getText(),
            fragilityChooser.selectedProperty().getValue(),
            Float.parseFloat(fromList.valueProperty().getValue().getLat()),
            Float.parseFloat(fromList.valueProperty().getValue().getLng()),
            Float.parseFloat(toList.valueProperty().getValue().getLat()),
            Float.parseFloat(toList.valueProperty().getValue().getLng()),
            classChoiceBox.getValue());
            // Package added to storage.
            packages.add(p1);
            // Information below added to usageLog.
            usageLog.add("Added package containing: " + nameField.getText()
                    +" Going from: "+fromList.getValue() +" to: " + toList.getValue()
                    +" Storage containing: "+packages.size()+"\n");
        
        } else {
            // If even one of the rules introduced in the beginning are not followed, one and the same error -message is shown.
            // Error message is shown in pop Up -window. Error message contains the rules for the user to review.
            try {
                
                errorText = "Something went wrong. Check following requirements: \n" +
                            "- If you chose item from the list, fill 'to' and 'from' boxes.\n" +
                        "- If you made your own item, make sure that:\n"
                        + "   - Name is added, volume is added, weight is added.\n"
                        + "   - 'to ' and 'from' boxes are filled.\n"
                        + "- Make sure you have chosen item from to list OR made your own, not both:\n"
                        + "   - If making your own, list must be empty.\n"
                        + "   - If choosing from the list, name, weight and volume must be empty.\n"
                        + "   - Volume and weight fields must contain float or int, not string.\n "
                        + "- Make sure that 'to' and 'from' is different place"; 
                Stage popUp = new Stage();
                Parent page = FXMLLoader.load(getClass().getResource("popUp.fxml"));
                Scene scene = new Scene(page);
                popUp.setTitle("Error");
                popUp.setScene(scene);
                popUp.show();
            
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }

    @FXML
    private void closeWindowAction(ActionEvent event) {
        
        // Window closes
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
    
}
