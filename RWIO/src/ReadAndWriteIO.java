/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 *
 * @author jannepenttila
 */
public class ReadAndWriteIO {

    private String filename, output;
    private String s;

    public ReadAndWriteIO(String s, String b) {
        filename = s;
        output = b;
    }
    



    public String readAndWrite() throws FileNotFoundException, IOException {
        try {
            ZipFile zf = new ZipFile("zipinput.zip");
            Enumeration entries = zf.entries();
            while (entries.hasMoreElements()) {
                ZipEntry ze = (ZipEntry) entries.nextElement();
                BufferedReader br = new BufferedReader(new InputStreamReader(zf.getInputStream(ze)));
                BufferedWriter out = new BufferedWriter(new FileWriter(output));
                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                    out.write(line);
                    out.write("\n");
                }
                br.close();
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        BufferedReader in = new BufferedReader(new FileReader(filename));
//        BufferedWriter out = new BufferedWriter(new FileWriter(output));
//        while (in.ready() == true){
//            String s = in.readLine();
//            //if (s.length() < 30 && s.length() > 1 && s.equals("  ") == false && s.contains("v") == true){
//            out.write(s);
//            out.write("\n");
//            //System.out.println(s);
//            //}
//        }
//        in.close();
//        out.close();
        
        return s;

    }

}
