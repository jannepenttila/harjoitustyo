package BottleDisp;




import java.util.ArrayList;
import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jannepenttila
 */
public class Bottle {
    
    protected String name;
    protected double size;
    protected double price;
    protected int number;
    static private Bottle b = null;
    

    
    
    public Bottle(String n, double p, int l) {
        name = n;
        //size = s;
        price = p;
        number = l;

    }
    
//        static public BottleDispenser getInstance(){
//        if (b == null)
//            b = new BottleDispenser ();
//        return b;
//    }
//    
    
    public void deleteBottle(int number) {
        number -= 1; 
    }
    
    public int getNumber (){
        return number;
    }
    
    public String getName () {
        return name;
    
    }
    
    public String getSize() {
        String size2 = String.valueOf(size);
        return size2;

    }
    
    public String getPrice(){
        String price2 = String.valueOf(price);
        return price2;

    }
    
    @Override
    public String toString() {
        return ("Name:"+this.getName()+
                " Size: "+ this.getSize() +
                " Price: "+ this.getPrice());
    }
}
    

