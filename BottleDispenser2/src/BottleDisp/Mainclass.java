package BottleDisp;


import java.util.Scanner;
import bdfx22.FXMLDocumentController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jannepenttila
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main() {
        
        BottleDispenser b1 = BottleDispenser.getInstance();

        Bottle b2 = new Bottle("Fanta Zero 0.5l", 1.95, 1);
        Bottle b3 = new Bottle("Fanta Zero 0.5l", 1.95, 1);
        Bottle b4 = new Bottle("Coca Cola Zero 1.5l", 2.5, 1);
        Bottle b5 = new Bottle("Coca Cola Zero 0.5l", 2.0, 1);
        Bottle b6 = new Bottle("Pepsi Max 1.5l", 2.2, 1);
        Bottle b7 = new Bottle("Pepsi Max 0.5l", 1.8, 1);

        
        b1.addBottle(b7);

        b1.addBottle(b6);

        b1.addBottle(b5);

        b1.addBottle(b4);

        b1.addBottle(b3);

        b1.addBottle(b2);
        
    }    
        
        
        
      
    
    
}
