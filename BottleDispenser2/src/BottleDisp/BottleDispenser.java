package BottleDisp;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Scanner;
import bdfx22.FXMLDocumentController;
import BottleDisp.Bottle;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jannepenttila
 */
public class BottleDispenser {
    
    private ArrayList<String> bottlelist = new ArrayList();
    
    

    private float money;
    private int apuri;
    static private BottleDispenser bd = null;
    protected int amount;
    public String fileName;
    public String text2File;
    
    
    
    public void addBottle(Bottle e) {
        

        bottlelist.add(e.getName());

        bottlelist.add(e.getPrice());
    }
    
    private BottleDispenser() {

        money = 0;

    }
    
    static public BottleDispenser getInstance(){
        if (bd == null)
            bd = new BottleDispenser ();
        return bd;
    }
    
    
    public String getName(String name){
        return name;
    }
    
    public float getPrice(float price){
        return price;
    }
    
    public double getSize(double size){
        return size;
    }

    public String addMoney(int amount) {
        String tuloste;
        money += amount;
        tuloste = ("Klink! Lisää rahaa laitteeseen "+amount+" €"+ "!");
        System.out.println(tuloste);
        return tuloste;
    }
    
    public String returnMoney() {
        String tuloste = null;
        NumberFormat formatter = new DecimalFormat("#0.00");
        String rahaaulos = formatter.format(money);
        String uusi = rahaaulos.replace(".", ",");
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + uusi + ("€"));
        tuloste= "Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + uusi + ("€");
        money = 0;
        return tuloste;
    }
    
    public String printAllBottles() {
        String tuloste = null;
        if (bottlelist.size() == 0){
            tuloste= "Kone on tyhjä.";
            System.out.println(tuloste);
        }
        else{
            for (int i = 0; i < bottlelist.size() / 2; i++) {
                System.out.print((i + 1) + ". " + "Nimi: " + bottlelist.get(i + i) + "\n");
                System.out.print("Hinta: " + bottlelist.get(i + 1 + i) + "\n");
    //            tuloste = ((i+1) + ". " + "Nimi: " + bottlelist.get(i+i) + "\n" +
    //                    "	" +"Hinta: " + bottlelist.get(i+1+i) + "\n");
            }
            System.out.print("Syötä valintasi numero. ");
        }
        return tuloste;
    }
    
    public String buyer(String brand, Double size) {
        
        String sold = null;
        String choosedBrand;
        String invoice = null;
        Double choosedSize;
        choosedBrand = brand;
        choosedSize = size;
        
        if (choosedBrand == "Pepsi Max" && choosedSize == 0.5) {
            if (bottlelist.contains("Pepsi Max 0.5l") == true){
                apuri = bottlelist.indexOf("Pepsi Max 0.5l");
            }
            else{
               apuri = 600;
            }
          
        } else if (choosedBrand == "Pepsi Max" && choosedSize == 1.5) {
            if (bottlelist.contains("Pepsi Max 1.5l") == true) {
                apuri = bottlelist.indexOf("Pepsi Max 1.5l");
            } else {
                apuri = 600;
            }

        } else if (choosedBrand == "Coca Cola Zero" && choosedSize == 0.5) {
            if (bottlelist.contains("Coca Cola Zero 0.5l") == true) {
                apuri = bottlelist.indexOf("Coca Cola Zero 0.5l");
            } else {
                apuri = 600;
            }

        } else if (choosedBrand == "Coca Cola Zero" && choosedSize == 1.5) {
            if (bottlelist.contains("Coca Cola Zero 1.5l") == true) {
                apuri = bottlelist.indexOf("Coca Cola Zero 1.5l");
            } else {
                apuri = 600;
            }
           

        } else if (choosedBrand == "Fanta Zero" && choosedSize == 0.5) {
            if (bottlelist.contains("Fanta Zero 0.5l") == true) {
                apuri = bottlelist.indexOf("Fanta Zero 0.5l");
            } else {
                apuri = 600;
            }

        }
        
        if (apuri==600){
            sold = "Eipä ole tuota pulloa!";
        
        }
        
        else{
            float phinta;
            phinta = Float.parseFloat(bottlelist.get(apuri + 1));
            if (money < phinta) {
                System.out.println("Syötä rahaa ensin!");
            } else if (money > phinta) {
                money -= phinta;
                System.out.println("KACHUNK! " + bottlelist.get(apuri) + " tipahti masiinasta!");
                sold =  "KACHUNK! " + bottlelist.get(apuri) + " tipahti masiinasta!";
                invoice = "Ostelit pullon " + bottlelist.get(apuri) + "." + "\n"
                        + "\n"+"Hintaa pullolla oli " + phinta + " €.";
                bottlelist.remove(apuri + 1);
                bottlelist.remove(apuri);
                
                fileName = "kuitti.txt";
                text2File = "\n"+"Tämä on kuitti. " +"\n"+"\n"+ invoice+"\n";
                File file = new File(fileName);
                FileWriter writer;
                try {
                    writer = new FileWriter(file, true);
                    PrintWriter printer = new PrintWriter(writer);
                    printer.append(text2File);
                    printer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sold;
    }
    
    public void invoicePrinter(String invoice){
        fileName = "kuitti.txt";
        text2File = "Tämä on kuitti. Ostelit pullon " + invoice + "." + "\n";
        File file = new File(fileName);
        FileWriter writer;
        try {
            writer = new FileWriter(file, true);
            PrintWriter printer = new PrintWriter(writer);
            printer.append(text2File);
            printer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    


    
}
   
