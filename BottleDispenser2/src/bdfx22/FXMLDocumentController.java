/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdfx22;

import BottleDisp.Bottle;
import BottleDisp.BottleDispenser;
import BottleDisp.Mainclass;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

/**
 *
 * @author jannepenttila
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Label machineLabel;
    @FXML
    private Label printLabel;
    @FXML
    private Button moneyOutButton;
    @FXML
    private Button chooseButton;
    @FXML
    private Button moneyInButton;
    @FXML
    private Slider moneySlider;
    
    int amount;
    
    int valinta;
    @FXML
    private Button buyButton;
    
    @FXML
    private ChoiceBox<String> brandChooseBox;
    @FXML
    private ChoiceBox<Double> sizeChooseBox;
    
    BottleDispenser b1 = BottleDispenser.getInstance();
    @FXML
    private Button printInvoiceButton;
    
    
    
    private void handleButtonAction(ActionEvent event) {
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        BottleDisp.Mainclass.main();

        machineLabel.setText("*** LIMSA-AUTOMAATTI ***");
        
        brandChooseBox.setItems(FXCollections.observableArrayList("Pepsi Max", "Coca Cola Zero", "Fanta Zero"));
        
        sizeChooseBox.setItems(FXCollections.observableArrayList(0.33, 0.5, 1.5));
        
        
      
        
        
    }    

    @FXML
    private void moneyOutAction(ActionEvent event) {
        printLabel.setText(b1.returnMoney());
    }

    @FXML
    private void chooseAction(ActionEvent event) {
        String tuloste = null;
        printLabel.setText(b1.printAllBottles());
    }

    @FXML
    private void moneyInAction(ActionEvent event) {
        amount = (int)moneySlider.getValue();
        printLabel.setText(b1.addMoney(amount));
        moneySlider.setValue(0.0);
        
    }

    @FXML
    private String buyBottleAction(ActionEvent event) {
        
        String brand = null;
        String sold = null;
        double size = 0;

        brand = brandChooseBox.getValue();
        size = sizeChooseBox.getValue();
        printLabel.setText(b1.buyer(brand, size) );
        return sold;
        
    }

    @FXML
    private void printInvoiceAction(ActionEvent event) {
//        b1.invoicePrinter();
        printLabel.setText("Tallennettu tiedostoon kuitti.txt.");
    }
    
}
