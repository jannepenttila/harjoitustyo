/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko6t5;

import java.util.Scanner;

/**
 *
 * @author jannepenttila
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Character c1 = new Character("Janne", "Glock");
    
    while (true) {
        System.out.println("*** TAISTELUSIMULAATTORI ***");
        System.out.println("1) Luo hahmo\n" +"2) Taistele hahmolla\n" +"0) Lopeta");
        Scanner scan = new Scanner(System.in).useDelimiter("\\n");
        System.out.print("Valintasi: ");
        int valinta;
        valinta = scan.nextInt();
        if (valinta == 1) {
            c1.createCharacter();
            c1.chooseWeapon();
        } else if (valinta == 2) {
            c1.fight();
        } else if (valinta == 0) {
            break;
        } else {
            System.out.println("Tuntematon valinta");
        }
    }
    }
    
}
