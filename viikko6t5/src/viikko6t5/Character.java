/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko6t5;

import java.util.Scanner;

/**
 *
 * @author jannepenttila
 */
public class Character {
    
    private String name;
    private String weapon;
    
    
    public Character (String n, String w){
        name = n;
        weapon = w;
    }
    
    public String createCharacter(){
        while (true){
            System.out.println("Valitse hahmosi: " + "\n" + "1) Kuningas" +"\n"+ "2) Ritari" +"\n"+
                    "3) Kuningatar" +"\n"+ "4) Peikko");
        Scanner scan = new Scanner(System.in).useDelimiter("\\n");
        System.out.print("Valintasi: ");
        int valinta;
        valinta = scan.nextInt();
        if (valinta == 1){
            name = "King";
            return name;
        }
        else if (valinta == 2){
            name = "Knight";
            return name;
        }
        else if (valinta == 3){
            name = "Queen";
            return name;
        }
        else if (valinta == 4){
            name = "Troll";
            return name;
        }
        else {
            System.out.println("Tuntematon valinta");
        }
        }
    }
    
    public String chooseWeapon(){
        while (true) {
            System.out.println("Valitse aseesi: " + "\n" + "1) Veitsi" + "\n" + "2) Kirves" + "\n"
                    + "3) Miekka" + "\n" + "4) Nuija");
            Scanner scan = new Scanner(System.in).useDelimiter("\\n");
            System.out.print("Valintasi: ");
            int valinta;
            valinta = scan.nextInt();
            if (valinta == 1) {
                weapon = "Knife";
                return weapon;
            } else if (valinta == 2) {
                weapon = "Axe";
                return weapon;
            } else if (valinta == 3) {
                weapon = "Sword";
                return weapon;
            } else if (valinta == 4) {
                weapon = "Club";
                return weapon;
            } else {
                System.out.println("Tuntematon valinta");
            }
        }
    }
    
    public void fight(){
        System.out.println(name + " tappelee aseella " + weapon);
    }
    
}
